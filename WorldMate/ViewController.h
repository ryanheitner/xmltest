//
//  ViewController.h
//  WorldMate
//
//  Created by Ryan Heitner on 22/12/2013.
//  Copyright (c) 2013 Ryan Heitner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBookUI/AddressBookUI.h>

@interface ViewController : UIViewController <ABPeoplePickerNavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *wmButton;
- (IBAction)wmButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *firstName;
@property (weak, nonatomic) IBOutlet UILabel *emailAddress;

@end
