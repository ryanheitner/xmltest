//
//  PartyParser.h
//  XMLTest
//
//  Created by Ryan Heitner on 23/12/2013.
//  Copyright (c) 2013 Ryan Heitner. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Party;

@interface PartyParser : NSObject
+ (Party *)loadParty;
+ (void)saveParty:(Party *)party ;


@end
