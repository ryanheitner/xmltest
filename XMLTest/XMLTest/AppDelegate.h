//
//  AppDelegate.h
//  XMLTest
//
//  Created by Ryan Heitner on 23/12/2013.
//  Copyright (c) 2013 Ryan Heitner. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Party;
@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate> 
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) IBOutlet ViewController *viewController;
@property (nonatomic, retain) Party *party;

- (void)saveParty;
@end
