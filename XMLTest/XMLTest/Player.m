#import "Player.h"

@implementation Player


- (id)initWithName:(NSString *)name level:(int)level rpgClass:(RPGClass)rpgClass {
    
    if ((self = [super init])) {
        self.name = name;
        self.level = level;
        self.rpgClass = rpgClass;
    }
    return self;
    
}

- (void) dealloc {
    self.name = nil;
}

@end