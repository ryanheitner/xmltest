//
//  Party.h
//  XMLTest
//
//  Created by Ryan Heitner on 23/12/2013.
//  Copyright (c) 2013 Ryan Heitner. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Party : NSObject

@property (nonatomic, retain) NSMutableArray *players;


@end
