//
//  MyData.h
//  WorldMate
//
//  Created by Ryan Heitner on 23/12/2013.
//  Copyright (c) 2013 Ryan Heitner. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyData : NSObject <NSXMLParserDelegate>

-(BOOL)parseDocumentWithURL:(NSURL *)url;

@end
