//
//  Party.m
//  XMLTest
//
//  Created by Ryan Heitner on 23/12/2013.
//  Copyright (c) 2013 Ryan Heitner. All rights reserved.
//

#import "Party.h"

@implementation Party
- (id)init {
    
    if ((self = [super init])) {
        self.players = [[NSMutableArray alloc] init];
    }
    return self;
    
}

- (void) dealloc {
    self.players = nil;
}

@end
