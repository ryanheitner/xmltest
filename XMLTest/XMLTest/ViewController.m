//
//  ViewController.m
//  XMLTest
//
//  Created by Ryan Heitner on 23/12/2013.
//  Copyright (c) 2013 Ryan Heitner. All rights reserved.
//

#import "ViewController.h"
#import "PartyParser.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)savePlayer:(id)sender {
    NSLog(@"saved");
    // this is an easy way to call a method in the app delegate
    [[[UIApplication sharedApplication] delegate] performSelector:@selector(saveParty)];

}
@end
